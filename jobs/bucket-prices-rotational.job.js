const db = require("../models");
const Bucket = db.Bucket;
const BucketToken = db.BucketToken;
const Token = db.Token;
const BucketPortfolioHistoriesRotational = db.BucketPortfolioHistoriesRotational;

const getCurrentBucketValue = require('../helpers/current-bucket-price.helper.js')


module.exports = async function bucketPricesJob() {
    try{
        let buckets = await Bucket.findAll({include : {model: BucketToken, include: {model: Token}}})
        
        for (let bucket of buckets) {

            //Check if count exceeds, if yes delete first count record
            let recordsCount=await BucketPortfolioHistoriesRotational.count({where: {bucket_id : bucket.id}})
            
            if(recordsCount>144){
                //delete current first entry for the history record
                let firstRecord = await BucketPortfolioHistoriesRotational.findOne({where : {bucket_id : bucket.id}, order: [ [ 'createdAt', 'ASC' ]]})
                await BucketPortfolioHistoriesRotational.destroy({where: {id : firstRecord.id}})
            }

            //let resultBuy = await getCurrentBucketValue(bucket)
            let resultSell = await getCurrentBucketValue(bucket,'sell')
            
            if(resultSell.status=='success'){
                resultSell = resultSell.bucketValue

                //console.log(profit_value)
                let bucketPortfolioHistory = {
                    'bucket_id' : bucket.id,
                    'current_value' : resultSell
                }
                
                //console.log(bucketPortfolioHistory)
                await BucketPortfolioHistoriesRotational.create(bucketPortfolioHistory)
            }
        }
    }catch (err) {
            console.log(err)

    }
}
