const db = require("../models");
const Token = db.Token;
const TokenHistory = db.TokenHistory;

const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')

module.exports = async function tokenPricesJob() {
    try{

        //get all tokens
        let tokens = await Token.findAll()
        console.log(tokens)

        for (let token of tokens) {

            let cryptoPrice = await buySellModuleAPI.getPrice(token.base_value_vauld,'sell',token.token_abbr.toLowerCase())
            if(cryptoPrice){
                console.log(cryptoPrice.data.quoteAmount)
                let tokenHistory = {
                    "token_id" : token.id,
                    "current_value" : cryptoPrice.data.quoteAmount
                }

                await TokenHistory.create(tokenHistory);
            }
        }

    }catch (err) {
        console.log(err)

    }
}