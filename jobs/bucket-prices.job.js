const db = require("../models");
const Bucket = db.Bucket;
const BucketToken = db.BucketToken;
const Token = db.Token;
const BucketPortfolioHistory = db.BucketPortfolioHistory;

const getCurrentBucketValue = require('../helpers/current-bucket-price.helper.js')


module.exports = async function bucketPricesJob() {
    try{
        let buckets = await Bucket.findAll({include : {model: BucketToken, include: {model: Token}}})
        
        for (let bucket of buckets) {

            //let resultBuy = await getCurrentBucketValue(bucket)
            let resultSell = await getCurrentBucketValue(bucket,'sell')
            
            if(resultSell.status=='success'){
                resultSell = resultSell.bucketValue

                //console.log(profit_value)
                let bucketPortfolioHistory = {
                    'bucket_id' : bucket.id,
                    'current_value' : resultSell
                }
                
                //console.log(bucketPortfolioHistory)
                await BucketPortfolioHistory.create(bucketPortfolioHistory)
            }
        }
    }catch (err) {
            console.log(err)

    }
}
