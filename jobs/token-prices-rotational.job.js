const db = require("../models");
const Token = db.Token;
const TokenHistoriesRotational = db.TokenHistoriesRotational;

const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')

module.exports = async function tokenPricesJob() {
    try{

        //get all tokens
        let tokens = await Token.findAll()
        //console.log(tokens)

        for (let token of tokens) {

            //Check if count exceeds, if yes delete first count record
            let recordsCount=await TokenHistoriesRotational.count({where: {token_id : token.id}})
            
            if(recordsCount>144){
                //delete current first entry for the history record
                let firstRecord = await TokenHistoriesRotational.findOne({where : {token_id : token.id}, order: [ [ 'createdAt', 'ASC' ]]})
                await TokenHistoriesRotational.destroy({where: {id : firstRecord.id}})
            }

            let cryptoPrice = await buySellModuleAPI.getPrice(token.base_value_vauld,'sell',token.token_abbr.toLowerCase())
            if(cryptoPrice){
                console.log(cryptoPrice.data.quoteAmount)
                let tokenHistory = {
                    "token_id" : token.id,
                    "current_value" : cryptoPrice.data.quoteAmount
                }

                await TokenHistoriesRotational.create(tokenHistory);
            }
        }

    }catch (err) {
        console.log(err)

    }
}