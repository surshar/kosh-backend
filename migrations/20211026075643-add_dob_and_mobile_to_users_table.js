'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Users', 'dob', {
      type: Sequelize.DATEONLY,
      after: "email"
    }),
    queryInterface.addColumn('Users', 'mobile_no', {
      type: Sequelize.STRING,
      after: "dob",
      unique: true
    })
  ];

  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users','dob'),
      queryInterface.removeColumn('Users','mobile_no'),
      ]);
  }
};
