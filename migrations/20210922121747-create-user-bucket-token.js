'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserBucketTokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      user_bucket_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      token_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      quantity: {
        allowNull: false,
        type: Sequelize.DECIMAL(50,20)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserBucketTokens');
  }
};