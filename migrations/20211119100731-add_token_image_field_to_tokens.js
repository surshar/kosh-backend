'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Tokens', 'token_image', {
      type: Sequelize.STRING,
      after: "token_name"
    })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Tokens', 'token_image'),
    ])
  }
};
