'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('Tokens', 'token_description', {
          type: Sequelize.TEXT,
          allowNull: true,
      })
   ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('Tokens', 'token_description', {
          type: Sequelize.STRING,
          allowNull: true,
      })
    ])
  }
};
