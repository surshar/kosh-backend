'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Admins', 'otp', {
      type: Sequelize.STRING,
      after: "password"
    }),
    queryInterface.addColumn('Admins', 'otp_attempts', {
      type: Sequelize.INTEGER,
      after: "otp"
    }),
    queryInterface.addColumn('Admins', 'otp_expiry', {
      type: Sequelize.DATE,
      after: "otp_attempts"
    })
  ];
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Admins','otp'),
      queryInterface.removeColumn('Admins','otp_attempts'),
      queryInterface.removeColumn('Admins','otp_expiry')
      ]);
  }
};
