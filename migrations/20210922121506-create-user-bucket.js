'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserBuckets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      user_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      bucket_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      investment_value: {
        allowNull: false,
        type: Sequelize.DECIMAL(50,20)
      },
      bucket_quantity: {
        allowNull: false,
        type: Sequelize.DECIMAL(50,20)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserBuckets');
  }
};