'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Users', 'otp', {
      type: Sequelize.STRING,
      after: "password"
    }),
    queryInterface.addColumn('Users', 'otp_attempts', {
      type: Sequelize.INTEGER,
      after: "otp"
    }),
    queryInterface.addColumn('Users', 'otp_expiry', {
      type: Sequelize.DATE,
      after: "otp_attempts"
    })
  ];
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users','otp'),
      queryInterface.removeColumn('Users','otp_attempts'),
      queryInterface.removeColumn('Users','otp_expiry')
      ]);
    }
};
