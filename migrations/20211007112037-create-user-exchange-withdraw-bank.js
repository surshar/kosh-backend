'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserExchangeWithdrawBanks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      exchange_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      user_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      accountNumber: {
        allowNull: false,
        type: Sequelize.STRING
      },
      IFSC: {
        allowNull: false,
        type: Sequelize.STRING
      },
      account_id_with_exchange: {
        type: Sequelize.STRING
      },
      account_status_with_exchange: {
        type: Sequelize.STRING
      },
      account_error_with_exchange: {
        type: Sequelize.STRING
      },
      is_deleted: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserExchangeWithdrawBanks');
  }
};