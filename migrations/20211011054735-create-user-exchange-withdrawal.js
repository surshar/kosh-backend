'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserExchangeWithdrawals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      user_id: {
        type: Sequelize.BIGINT.UNSIGNED
      },
      exchange_id: {
        type: Sequelize.BIGINT.UNSIGNED
      },
      user_exchange_withdraw_bank_id: {
        type: Sequelize.BIGINT.UNSIGNED
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      txnID: {
        type: Sequelize.STRING
      },
      message: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserExchangeWithdrawals');
  }
};