'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Exchanges', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      exchange_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      exchange_description: {
        type: Sequelize.TEXT
      },
      test_api_key: {
        type: Sequelize.STRING
      },
      production_api_key: {
        type: Sequelize.STRING
      },
      org_id: {
        type: Sequelize.STRING
      },
      status: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        default: false
      },
      created_by: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      last_updated_by: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Exchanges');
  }
};