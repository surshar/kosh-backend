'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Buckets', 'bucket_image', {
      type: Sequelize.STRING,
      after: "bucket_name"
    })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Buckets', 'bucket_image'),
    ])
    }
};
