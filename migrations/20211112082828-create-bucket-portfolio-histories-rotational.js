'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BucketPortfolioHistoriesRotationals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      bucket_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      current_value: {
        allowNull: false,
        type: Sequelize.DECIMAL(50,20)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BucketPortfolioHistoriesRotationals');
  }
};