'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Users', 'email_verification_code', {
      type: Sequelize.STRING,
      after: "user_verified"
    }),
    queryInterface.addColumn('Users', 'email_verification_code_expiry', {
      type: Sequelize.DATE,
      after: "email_verification_code"
    }),
    queryInterface.addColumn('Users', 'user_email_verified', {
      type: Sequelize.BOOLEAN,
      defaultValue: 0,
      after: "email_verification_code_expiry"
    })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users','email_verification_code'),
      queryInterface.removeColumn('Users','email_verification_code_expiry'),
      queryInterface.removeColumn('Users','user_email_verified'),  
    ]);
  }
};
