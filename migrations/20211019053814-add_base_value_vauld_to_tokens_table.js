'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Tokens', 'base_value_vauld', {
      type: Sequelize.BIGINT.UNSIGNED,
      after: "token_abbr"
    })
  ];
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Tokens','base_value_vauld'),
    ])
  }
};
