'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('Users', 'user_verified', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      after: "otp_expiry"
    })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users','user_verified'),
      ]);
  }
};
