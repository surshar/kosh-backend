'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('UserExchanges', 'user_exchange_kyc_status', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      after: "user_exchange_identity"
    })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('UserExchanges','user_exchange_kyc_status'),
      ]);
  }
};
