'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Buckets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT.UNSIGNED
      },
      version_no: {
        allowNull: false,
        type: Sequelize.STRING
      },
      bucket_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      bucket_description: {
        type: Sequelize.TEXT
      },
      exchange_id: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      old_flag: {
        default: false,
        type: Sequelize.BOOLEAN
      },
      versions: {
        type: Sequelize.TEXT
      },
      created_by: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      last_updated_by: {
        allowNull: false,
        type: Sequelize.BIGINT.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Buckets');
  }
};