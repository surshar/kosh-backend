const jwt = require('jsonwebtoken');
const db = require("../models");
const User = db.User;

module.exports = function auth(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401).send({message: "Unauthorized"})
  
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      //req.user = user
      //console.log(user)
      User.findOne({ where : {'id' : user.id, 'email' : user.email} }).then(data => {
       
        //verification check for all APIs (under authentication) except auth routes
        
        if(!req.baseUrl.includes('/api/auth')){
          if(!(data.user_verified&&data.user_email_verified)){
            return res.status(403).send({
              message:
                "User not verified."
            });
          }
        }

        if(data){
          req.user = data;
          next()
        } else{
          return res.status(403).send({
            message:
              "Forbidden"
          });
        }

      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving authenticated information."
        });
      });

    })
  };