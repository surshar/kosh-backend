const jwt = require('jsonwebtoken');
const db = require("../models");
const Admin = db.Admin;

module.exports = function authAdmin(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
  
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      
      if (err) return res.sendStatus(403)
      if(!user.admin){
        return res.status(403).send({
          message:
            "Forbidden"
        });
      }
      
      Admin.findOne({ where : {'id' : user.id, 'email' : user.email} }).then(data => {
        if(data){
          req.admin = data;
          next()
        } else{
          return res.status(403).send({
            message:
              "Forbidden"
          });
        }
        
      })
      .catch(err => {
        return res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving authenticated information."
        });
      });

    })
  };