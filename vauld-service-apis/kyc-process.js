var axios = require('axios');
var preRequest = require('./pre-request.js')

module.exports = {
    initiateKYC: async function(userId, docType){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","docType":"'+docType+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/initiateKyc',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },

    reqUploadURLs: async function(userId){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'"}';

        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/getUploadUrl',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },

    verifyDocument: async function(userId,docType,frontOrBack){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","documentType":"'+docType+'","frontOrBack":"'+frontOrBack+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/verifyDoc',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },

    verifySelfie: async function(userId){

        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/verifySelfie',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data

    },

    instantKYCApproval: async function(userId){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/instantApproval',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },

    manualVerification: async function(userId){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/InitiateManualVerification',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data

    },
    
    kYCStatus: async function(userId){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/kyc/getKYCStatus',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },

    uploadKYCDocument: async function(url, fileData){
        
        const presignedS3Url = url

        try{

            var config = {
                method: 'put',
                url: presignedS3Url,
                headers: { 
                    'Content-Type': 'image/jpeg'
                },
                data : fileData
            };
        
            const response = await axios(config)
            
            return response.data
        
        } catch(e){console.log(e)
            console.log(e.response.data);
            console.log(e.response.status);
            console.log(e.response.headers);
        }

    }
}