var axios = require('axios');
//const { response } = require('express');
var preRequest = require('./pre-request.js')

module.exports = {
    userCreate: async function(newUserId){
    
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userIdentifier":"'+newUserId+'","isCorporate":false}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/users/create',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data

    },

    userDetails: async function(userID){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userID+'"}';

        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/users/details',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data

    },

    userTransactions: function(){
        
    },

    userList: function(){
        
    }
}