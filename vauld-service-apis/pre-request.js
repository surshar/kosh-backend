var CryptoJS = require('crypto-js')

module.exports = function preRequest(payload){
    const apiKey = process.env.VAULD_API_KEY; //api key
    //console.log('-----')
    console.log(payload)
    //console.log('-----')
    let URIencodedPayload = jsonToURI(JSON.parse(payload));
    
    let hmac = CryptoJS.HmacSHA256(URIencodedPayload, apiKey);
    let hash = hmac.toString(CryptoJS.enc.Hex);
    
    return hash;
}

function jsonToURI(jsonObj) {
    var output = '';
    var keys = Object.keys(jsonObj);
    keys.forEach(function(key) {
        output = output + key + '=' + jsonObj[key] + '&';
    });
    return output.slice(0, -1);
}