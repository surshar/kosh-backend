var axios = require('axios');
var preRequest = require('./pre-request.js')

module.exports = {
    submitDepositConfirmation: async function(userId, paymentId, amount){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","paymentID":"'+paymentId+'","amount":"'+amount+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/PaymentConfirmed',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },
    addBankAccount: async function(userId,accountName,accountNumber,ifsc){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","name":"'+accountName+'","accountNumber":"'+accountNumber+'","ifsc":"'+ifsc+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/addBankAccount',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },
    requestWithdrawal: async function(userId,bankAccountID,amount){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","bankAccountID":"'+bankAccountID+'","amount":"'+amount+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/requestWithdrawal',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },
    removeBankAccount: async function(userId,bankAccountID){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userId+'","bankAccountID":"'+bankAccountID+'","amount":"'+amount+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/requestWithdrawal',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        
        return response.data
    }
    
}