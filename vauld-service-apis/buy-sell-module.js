var axios = require('axios');
var preRequest = require('./pre-request.js')

module.exports = {
    getPrice: async function(amount,type,token){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","amount":"'+amount+'","type":"'+type+'","token":"'+token+'"}'
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/getPrice',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    }
    ,
    getAllPrice: async function(){
        
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'"}';
        
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/getAllPrice',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },
    createOrder: async function(userID,type,token,inrAmount,tokenAmount){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userID+'","type":"'+type+'","token":"'+token+'","inrAmount":"'+inrAmount+'","tokenAmount":"'+tokenAmount+'"}';
        
        //data='{"orgID":"60d344b91b55b9001bd0bb89", "userID":"616f31dfcc3c910265554ba7", "type": "buy","token": "eth", "inrAmount": "1", "tokenAmount":"100000"}'
        console.log(data)

        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/createOrder',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    },
    orderStatus: async function(userID,orderID){
        var data = '{"orgID":"'+process.env.VAULD_ORG_ID+'","userID":"'+userID+'","orderID":"'+orderID+'"}';
        console.log(data)
        let payload = data;
        
        const hash=preRequest(payload)

        var config = {
        method: 'post',
        url: process.env.VAULD_API_URL+'/fiat/details',
        headers: { 
            'Content-Type': 'application/json',
            'hmac' : hash
        },
        data : data
        };

        const response = await axios(config)
        return response.data
    }
}