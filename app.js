require('dotenv').config();
const express = require('express');
var cors = require('cors');
const app = express();

app.use(cors())
app.use(express.json())
app.use(express.static('uploads/public'))

require("./routes/authadmin.route")(app);
require("./routes/auth.route")(app);
require("./routes/user.route")(app);
require("./routes/deposit-withdraw.route")(app);
require("./routes/user-bucket.route")(app);
require("./routes/token.route")(app);
require("./routes/bucket.route")(app);
require("./routes/graph.route")(app);

// set port, listen for requests
let PORT=null
if(process.env.NODE_ENV != 'test'){
  PORT = process.env.PORT || 8080;

  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
  });

} else{
  PORT = process.env.PORT_TEST || 8081;
}

module.exports = app

