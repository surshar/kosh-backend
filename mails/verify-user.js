var mail = require('./send-mail.js')

module.exports = function verifyUser(email,otp){
    let subject = 'Account verify';
    let message = 'Welcome, Your OTP for verifying your account is '+otp;
    mail(email,subject,message)
}