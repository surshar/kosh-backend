var nodemailer = require('nodemailer');

module.exports = function send(email,subject,message){
    var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_ID,
        pass: process.env.EMAIL_PASS
    }
    });

    var mailOptions = {
    from: process.env.EMAIL_ID,
    to: email,
    subject: subject,
    html: message
    };

    transporter.sendMail(mailOptions, function(error, info){
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
    });
}