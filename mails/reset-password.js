var mail = require('./send-mail.js')

module.exports = function resetPassword(email,otp){
    let subject = 'Account Password Reset';
    let message = 'Your OTP for resetting password is '+otp;
    mail(email,subject,message)
}