var mail = require('./send-mail.js')

module.exports = function resetPassword(email){
    let subject = 'Account Password Reset Confirmation';
    let message = 'Password of your account has been reset successfully.'; 
    mail(email,subject,message)
}