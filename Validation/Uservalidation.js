const {check,validationResult } = require('express-validator');
const db = require("../models")
const User = db.User;

const findEmail = require("../controllers/user.controller")
exports.userValidatorResult=(req,res,next)=>{
    const result = validationResult(req)
    if(!result.isEmpty()){
        const error = result.array()[0].msg
        return res.status(422).json({success:false,error:error})
        }
    next()
}
//Validation of name,Password ,email checking ,character length
exports.userValidator=[
    check('firstName')
    .trim().not()
    .isEmpty().withMessage("Firstname is required")
    .isLength({min:3,max:20})
    .withMessage("Firstname must be 3 to 150 characters long"),

    check('lastName')
    .trim().not()
    .isEmpty().withMessage("Lastname is required")
    .isLength({min:3,max:20})
    .withMessage("Lastname must be 3 to 150 characters long"),

    check('email')
    .trim().not()
    .isEmpty().withMessage("Email is required")
    .isEmail()
    .withMessage("Please Enter a valid Email")
    .bail()
    .custom(async(email)=>{
        const user = await findEmail.findByEmail(email)
        if(user){
          throw new Error('Email Already in use');
        }
    }),

    check('password')
    .trim().not()
    .isEmpty().withMessage("Password is required")
    .isLength({min:8})
    .withMessage("Password must be atleast 8  characters long"),
    
    check('confirmPassword').trim().not().isEmpty().withMessage('Confirm password required').custom((value,{req})=>{
            if(value !== req.body.password){
                throw new Error("Password and confirm password do not Match")
            }
            return true;
    })
]

