//const moment = require('moment');

const db = require("../models")
const Bucket = db.Bucket;
const BucketToken = db.BucketToken;
const Token = db.Token;
const BucketPortfolioHistory = db.BucketPortfolioHistory;

const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')

exports.create = async (req,res) => {
    
    let transaction = false;    
    try {
        // get transaction
        transaction = await db.sequelize.transaction();

        let bucket = { 
                    bucket_name : req.body.bucket_name,
                    bucket_description : req.body.bucket_description,
                    exchange_id : 1,
                    created_by : req.admin.id,
                    last_updated_by : req.admin.id,
                    version_no : 1
        }

        bucket = await Bucket.create(bucket,{transaction: transaction})

        for (const bucketTokenReq of req.body.bucket_tokens) {
            let bucketToken = {
                "bucket_id" : bucket.id,
                "token_id" : bucketTokenReq.token_id,
                "quantity" : bucketTokenReq.quantity
            }

            await BucketToken.create(bucketToken,{transaction: transaction})
        }
        
        // commit
        await transaction.commit();

        bucket = await Bucket.findOne({include : {model: BucketToken, include: {model: Token}}, where: {id: bucket.id}})

        return res.send({'status':'success', 'data': bucket})
    } catch (err) {
        console.log(err)
        // Rollback transaction 
        if (transaction) await transaction.rollback();
    }
}

exports.addBucketImage = async (req,res) => {

    if(req.file){
        let bucket = await Bucket.findOne({where: {id: req.body.bucket_id}})
        bucket.bucket_image = req.folderName+'/'+req.file.filename
        await bucket.save()

        return res.send({'status':'success','message':'Image added to the bucket successfully'})
    } else{
        return res.send({'status':'error','message':'File required'})
    }
}

exports.findAll = async (req,res) => {
    let buckets = await Bucket.findAll({include : {model: BucketToken, include: {model: Token}}})
    //calculate and add prices for each bucket and their tokens
    return res.send({'status':'success', 'data': buckets})
} 

exports.findOne = async (req,res) => {
    try{
        let bucket = await Bucket.findOne({include : {model: BucketToken, include: {model: Token}}, where: {id: req.body.bucket_id}})
        bucket=bucket.toJSON()

        let bucketValueBuy=0
        let bucketValueSell=0
        //let bucketValueAt=moment()

        //bucket.BucketTokens.forEach(bucketToken => {
        for (let bucketToken of bucket.BucketTokens) {
            //get buy price for single token quantity
            let cryptoPriceBuy = await buySellModuleAPI.getPrice(bucketToken.Token.base_value_vauld,'buy',bucketToken.Token.token_abbr.toLowerCase())

            if(cryptoPriceBuy.success){
                cryptoPriceBuy=cryptoPriceBuy.data
                bucketToken.Token.buy_price = cryptoPriceBuy.quoteAmount
                bucketToken.total_buy_price = cryptoPriceBuy.quoteAmount*bucketToken.quantity
                bucketValueBuy += bucketToken.total_buy_price
                

            } else{
                return res.status(500).send({
                    message:
                        "Problem with "+bucketToken.Token.token_abbr+" not being found.",
                    status: "fail"
                });
            }

            //get sell price for token quantity
            let cryptoPriceSell = await buySellModuleAPI.getPrice(bucketToken.Token.base_value_vauld,'sell',bucketToken.Token.token_abbr.toLowerCase())
            console.log(bucketToken.Token.base_value_vauld)

            if(cryptoPriceSell.success){
                cryptoPriceSell=cryptoPriceSell.data
                bucketToken.Token.sell_price = cryptoPriceSell.quoteAmount
                bucketToken.total_sell_price = cryptoPriceSell.quoteAmount*bucketToken.quantity
                bucketValueSell += bucketToken.total_sell_price

            } else{
                return res.status(500).send({
                    message:
                        "Problem with "+bucketToken.Token.token_abbr+" not being found.",
                    status: "fail"
                });
            }
        }

        bucket.total_buy_price=bucketValueBuy
        bucket.total_sell_price=bucketValueSell

        //Calculate Growth Percent from its inception
        let bucketGraphData = await BucketPortfolioHistory.findOne({where: {bucket_id: bucket.id}, order: [ [ 'createdAt', 'ASC' ]]});

        bucket.growth_perct = null;
        
        if(bucketGraphData){
            bucket.diff_with = bucketGraphData.current_value
            bucket.growth_perct = ((bucket.total_sell_price - bucketGraphData.current_value) * 100) / bucketGraphData.current_value;
        }

        return res.send({'status':'success', 'data': bucket})
    }
    catch (err) {
        console.log(err)

    }
    
}