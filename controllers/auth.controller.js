const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const moment = require('moment');

var verificationMobile = require('../mails/verify-user.js')
var verificationEmail = require('../mails/verify-user-email.js')

var resetPasswordMail = require('../mails/reset-password.js')
var resetPasswordConfirmMail = require('../mails/reset-password-confirmation.js')

const db = require("../models")
const user = require('../models/user');
const User = db.User;
const RefreshToken = db.RefreshToken;

exports.register = async(req, res, next) => {
    try {
        //console.log(req.body);
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        
        // Create a User
        const userCreate = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            dob: req.body.dob,
            mobile_no: req.body.mobile_no,
            password: hashedPassword,
            user_verified: 1, //***IMPORTANT - Remove this when verification check needs to be activated */
            user_email_verified: 1, //***IMPORTANT - Remove this when verification check needs to be activated */
        } 
   
        // Save User in the database
        let user = await User.create(userCreate);

        let otpCode = Math.floor(100000 + Math.random() * 900000);
        //Store Verification OTP
        user.otp=otpCode;
        user.otp_attempts=0;
        user.otp_expiry=moment().add(1, 'days');
        user.save();
        
        //Store Verification Link
        let verify_token = require('crypto').randomBytes(64).toString('hex');
        user.email_verification_code = verify_token
        user.email_verification_code_expiry = moment().add(1, 'days');
        user.save();

        //Mobile verification code send
        verificationMobile(req.body.email,otpCode)

        //Email Verification link send
        let verify_link=req.protocol + '://' + req.hostname + ':' + process.env.PORT + '/' + (process.env.baseURL ? process.env.baseURL + '/' : '') + 'api/auth/verify-email?email=' + user.email + '&verify_token=' + verify_token
        console.log(verify_link)
        verificationEmail(req.body.email,verify_link)

        userMini = { id: user.id, email: user.email }
        const accessToken = generateAccessToken(userMini)
        const refreshToken = jwt.sign(userMini, process.env.REFRESH_TOKEN_SECRET)
        //add to Refresh Token table for the user
        const refreshTokenObj = {
            user_id : userMini.id,
            refresh_token : refreshToken
        }
      RefreshToken.create(refreshTokenObj)
          .then(data => {
          //res.send(data);
          return res.json({ accessToken: accessToken, refreshToken: refreshToken })
          })
          .catch(err => {
          return res.status(500).send({
              message:
              err.message || "Some error occurred while creating the User."
          });
          });
      
      } catch(e) {console.log(e)
        res.status(500).send(e)
      }

}

exports.login = (req, res) => {
    const user = User.unscoped().findOne({where : {email : req.body.email}}).then(async user => {
        if(await bcrypt.compare(req.body.password, user.password)) {

            const authUser = { id: user.id, email: user.email }
          
            const accessToken = generateAccessToken(authUser)
            const refreshToken = jwt.sign(authUser, process.env.REFRESH_TOKEN_SECRET)
            //add to Refresh Token table for the user
            const refreshTokenObj = {
                user_id : user.id,
                refresh_token : refreshToken
            }
            RefreshToken.create(refreshTokenObj)
                .then(data => {
                //res.send(data);
                return res.json({ accessToken: accessToken, refreshToken: refreshToken })
                })
                .catch(err => {
                return res.status(500).send({
                    message:
                    err.message || "Some error occurred while creating the User."
                });
                });
            
                

            
            } else {
              return res.send({'message':'Not Allowed'})
            }
    }).catch(err => {
        return res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving user."
          });
    });
    
}

exports.token = (req, res) => {

    const refreshToken = req.body.refreshToken

    //console.log(req.user);
    if (refreshToken == null) return res.sendStatus(401)
    RefreshToken.findAll({ attributes: ['refresh_token'] ,where: {user_id: req.user.id}}).then(data => {
        const refreshTokens = data.map(function(value) {
            return value.refresh_token;
          });
        if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403)
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)
        const accessToken = generateAccessToken({ email: user.email })
        res.json({ accessToken: accessToken })
        })
    })
}

exports.logout = (req, res) => {
    //refreshTokens = refreshTokens.filter(token => token !== req.body.refreshToken)
    //res.sendStatus(204)

    RefreshToken.destroy({ attributes: ['refresh_token'] ,where: {user_id: req.user.id,refresh_token: req.body.refreshToken}}).then(num => {console.log(num)
          return res.send({
            message: "Logout Successful"
          });
      })
      .catch(err => {
        return res.status(500).send({
          message: "Something went wrong"
        });
      });
}

exports.resetPassword = async (req,res) => {
  let data =await User.findOne({ where: {email:req.body.email} })

    if(data){

        let otpCode = Math.floor(100000 + Math.random() * 900000);
        //Store OTP
        data.otp=otpCode;
        data.otp_attempts=0;
        data.otp_expiry=moment().add(1, 'days');
        data.save();

        //Email send to User
        resetPasswordMail(req.body.email,otpCode)
        
        return res.status(200).send({message:"Please check your emails for OTP verification"})
    }else{
        return res.status(401).send({message:"No such User exists"})
    }
}

exports.changePassword =  async (req,res) => {
  let data =await User.findOne({ where: {email:req.body.email} })

  if(data){
    if(moment().isBefore(data.otp_expiry)){
      
      if(data.otp_attempts<2){
        
        if(data.otp==req.body.otp){
          data.otp=null
          data.otp_attempts=null
          data.otp_expiry=null
          data.password=await bcrypt.hash(req.body.password,10);
          data.save()

          resetPasswordConfirmMail(data.email)

          return res.status(200).send({message:"Password changed successfully. Please login with your  new password"})
        } else{
          data.otp_attempts+=1;
          data.save()

          return res.status(200).send({message:"Wrong OTP"})
        }
      
      } else{
        return res.status(200).send({message:"Attempts expired. Please generate the OTP again."})
      }

    } else{
      
      return res.status(200).send({message:"OTP Expired. Please generate the OTP again."})
    
    }
  } else{
    return res.status(401).send({message:"No such User exists"})
  }
}


exports.verifyUser =  async (req,res) => {

  data = req.user;

  if(data.user_verified){
    return res.status(200).send({message:"Account is already verified"})
  }

  if(moment().isBefore(data.otp_expiry)){
    
    if(data.otp_attempts<2){
      
      if(data.otp==req.body.otp){
        data.otp=null
        data.otp_attempts=null
        data.otp_expiry=null
        data.user_verified=1;
        data.save()

        return res.status(200).send({message:"Account verified successfully"})
      } else{
        data.otp_attempts+=1;
        data.save()

        return res.status(200).send({message:"Wrong OTP"})
      }
    
    } else{
      return res.status(200).send({message:"Attempts expired. Please generate the OTP again."})
    }

  } else{
    
    return res.status(200).send({message:"OTP Expired. Please generate the OTP again."})
  
  }
}

exports.generateOTP = async (req, res) => {
  let user =await User.findOne({ where: {email:req.body.email} })

  if(user){
    let otpCode = Math.floor(100000 + Math.random() * 900000);
    //Store OTP
    user.otp=otpCode;
    user.otp_attempts=0;
    user.otp_expiry=moment().add(1, 'days');
    user.save();

    if(req.body.otp_action == 'reset_request'){

      resetPasswordMail(req.body.email,otpCode)
      return res.send({message: "OTP has been sent succesfully."});
    }else if(req.body.otp_action == 'verify_request'){
      
      verificationMobile(req.body.email,otpCode)
      return res.send({message: "OTP has been sent succesfully."});
    }else {
      return res.status(401).send({message: "otp_action for is required."});
    }

    

  } else{
    return res.status(401).send({message:"No such User exists"})
  }
  
}

exports.verifyEmail = async (req, res) => {

  let email = req.query.email;
  let verify_token = req.query.verify_token;

  if(!verify_token){
    res.send.status(401).send({message: "Verify Token required"})
  }

  let data =await User.findOne({ where: {email:email} })

  if(data.user_email_verified){
    return res.status(200).send({message:"Email is already verified"})
  }

  if(moment().isBefore(data.email_verification_code_expiry)){
    
      if(data.email_verification_code==verify_token){
        data.email_verification_code=null
        data.email_verification_code_expiry=null
        data.user_email_verified=1;
        data.save()

        return res.status(200).send({message:"Email verified successfully"})
      } else{

        return res.status(200).send({message:"Invalid Link"})
      }

  } else{
    
    return res.status(200).send({message:"Verification link expired. Please generate again."})
  
  }

  
}


function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1000m' })
  }

exports.test = (req,res) => {
  return res.status(200).send({message: 'yes'})
}