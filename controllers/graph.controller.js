const db = require("../models");

const BucketPortfolioHistory = db.BucketPortfolioHistory;
const TokenHistory = db.TokenHistory;

exports.bucketGraph = async (req,res) => {
    //Bucket graph as per bucket id

    let bucketGraphData = await BucketPortfolioHistory.findAll({where: {bucket_id: req.body.bucket_id}});

    return res.send({'status':'success', 'data': bucketGraphData})
}

exports.tokenGraph = async (req,res) => {
    //Token graph as per token id


    let tokenGraphData = await TokenHistory.findAll({where: {token_id: req.body.token_id}});

    return res.send({'status':'success', 'data': tokenGraphData})
}