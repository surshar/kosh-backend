const db = require("../models");
const Token = db.Token;

const getCurrentTokenValue = require('../helpers/current-token-price.helper.js')

exports.findAll = async (req,res) => {
    let tokens = await Token.findAll()

    for (let token of tokens) {
        let response = await getCurrentTokenValue(token,"sell")
        token.dataValues.price = response.tokenValue
        
    }
    

    return res.send({'status':'success', 'data': tokens})
}