const db = require("../models");
const { v4: uuidv4 } = require('uuid');
const User = db.User;
const UserBucket = db.UserBucket;
const UserExchange = db.UserExchanges;
const Op = db.Sequelize.Op;
const userAPI = require('../vauld-service-apis/user.js');
const kYCAPI = require('../vauld-service-apis/kyc-process.js')
const buySellAPI =  require('../vauld-service-apis/buy-sell-module.js')
//const test = userAPI.test;
//const test2 = userAPI.test2;

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a User
  const user = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  };

  // Save User in the database
  User.create(user)
    .then(data => {
      return res.send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
    //test()
    //test2()
    User.findAll({ include : [UserBucket] })
      .then(data => {
        return res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving users."
        });
      });
};
//Email exists checking on database-validation
exports.findByEmail = async (email) => {
  const user=  await User.findOne({where: {email: email}});
  return user
}

// Find a single User with an id
exports.findOne = (req, res) => {
  
};

// Update a User by the id in the request
exports.update = (req, res) => {
  
};

exports.intiateKYC = async (req,res) => {
  //Check if vauld User Id exists, if not generate one from vauld
  //console.log(req.user);
  let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
  if(!userExchange)
  {//Generate User Id
    const newUserId=uuidv4() //Random User Id for our reference 
    
    const response = await userAPI.userCreate(newUserId);
    userExchange = await UserExchange.create({ user_id: req.user.id, exchange_id: 1, user_identity: newUserId, user_exchange_identity: response.data.userID })

    //let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } })
    
    
    console.log(response.data.userID)
    console.log(userExchange.user_exchange_identity)

  }
  
  //After having vauld User Id. check for KYC status with Vauld
  //let user = await User.findOne({ where : {'id' : req.user.id} })

  const userId=userExchange.user_exchange_identity;
  const verification_type=req.body.verification_type ? req.body.verification_type : 'aadhar';
  const kYCStatus = await kYCAPI.kYCStatus(userId);
  console.log(kYCStatus)
  
  if(kYCStatus.data.status=='open'){
    //if KYC not initiated, intiate it, //open status
    const initiateKYCres=await kYCAPI.initiateKYC(userId,verification_type);
    console.log(userId)
    if(initiateKYCres.success){
      const uploadURLsres=await kYCAPI.reqUploadURLs(userId)
      return res.send(uploadURLsres)
    } else{
      return res.send({"status":"Something went wrong"})
    }
  } else{
    return res.send({"status":"KYC not in open status. Your KYC verification might still be in progress."})
  }

  

  //If KYC initiated but pending , give documents for upload //pending status
}

exports.verifyDocument = async (req,res) => {
  let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
  if(!userExchange){
    return res.send({"status":"Initiate KYC first"})
  }
  const response = await kYCAPI.verifyDocument(userExchange.user_exchange_identity,req.body.doc_type,req.body.front_or_back);
  return res.send(response);
}

exports.verifySelfie = async (req,res) => {
  let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
  if(!userExchange){
    return res.send({"status":"Initiate KYC first"})
  }
  const response = await kYCAPI.verifySelfie(userExchange.user_exchange_identity);
  return res.send(response);
}

exports.instantKYCApproval = async(req,res) => {
  let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
  if(!userExchange){
    res.send({"status":"Initiate KYC first"})
  }
  const response = await kYCAPI.instantKYCApproval(userExchange.user_exchange_identity);
  console.log(response)
  if(response.success){
    userExchange.user_exchange_kyc_status = true;
    await userExchange.save();
  }
  return res.send(response);
}

exports.uploadKYCDocument = async(req,res) => {

  let url = req.body.url; //s3 URL for upload
  let fileData = req.file.buffer
  
  let response=await kYCAPI.uploadKYCDocument(url,fileData)

  return res.send(response)
}

exports.userExchangeDetails = async(req,res) => {
  let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 

  let response = await userAPI.userDetails(userExchange.user_exchange_identity);

  return res.send(response)
}
