const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const moment = require('moment');

const db = require("../models")
const Admin = db.Admin
const RefreshToken = db.RefreshToken

var resetPasswordMail = require('../mails/reset-password.js')
var resetPasswordConfirmMail = require('../mails/reset-password-confirmation.js')

exports.login = async (req, res) => {

    const admin = await Admin.unscoped().findOne({where : {email : req.body.email}})
    if(!admin){
        return res.send({'message':'Not Allowed'})
    }
    if(await bcrypt.compare(req.body.password, admin.password)){
        const authUser = { id: admin.id, email: admin.email, admin: true }
          
            const accessToken = generateAccessToken(authUser)
            const refreshToken = jwt.sign(authUser, process.env.REFRESH_TOKEN_SECRET)
            //add to Refresh Token table for the user
            const refreshTokenObj = {
                user_id : admin.id,
                refresh_token : refreshToken
            }
            await RefreshToken.create(refreshTokenObj)
            
            return res.json({ accessToken: accessToken, refreshToken: refreshToken })
    } else {
        return res.send({'message':'Not Allowed'})
    }
}

exports.resetPassword = async (req,res) => {
    let data =await Admin.findOne({ where: {email:req.body.email} })
  
      if(data){
  
          let otpCode = Math.floor(100000 + Math.random() * 900000);
          //Store OTP
          data.otp=otpCode;
          data.otp_attempts=0;
          data.otp_expiry=moment().add(1, 'days');
          data.save();
  
          //Email send to User
          resetPasswordMail(req.body.email,otpCode)
          
          return res.status(200).send({message:"Please check your emails for OTP verification"})
      }else{
          return res.status(401).send({message:"No such User exists"})
      }
  }
  
  exports.changePassword =  async (req,res) => {
    let data =await Admin.findOne({ where: {email:req.body.email} })
  
    if(data){
      if(moment().isBefore(data.otp_expiry)){
        
        if(data.otp_attempts<2){
          
          if(data.otp==req.body.otp){
            data.otp=null
            data.otp_attempts=null
            data.otp_expiry=null
            data.password=await bcrypt.hash(req.body.password,10);
            data.save()
  
            resetPasswordConfirmMail(data.email)
  
            return res.status(200).send({message:"Password changed successfully. Please login with your  new password"})
          } else{
            data.otp_attempts+=1;
            data.save()
  
            return res.status(200).send({message:"Wrong OTP"})
          }
        
        } else{
          return res.status(200).send({message:"Attempts expired. Please generate the OTP again."})
        }
  
      } else{
        
        return res.status(200).send({message:"OTP Expired. Please generate the OTP again."})
      
      }
    } else{
      return res.status(401).send({message:"No such User exists"})
    }
  }

function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1000m' })
}