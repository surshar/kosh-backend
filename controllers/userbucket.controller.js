const moment = require('moment');

const db = require("../models");
const User = db.User;
const Bucket = db.Bucket;
const BucketToken = db.BucketToken;
const UserBucket = db.UserBucket;
const UserBucketToken = db.UserBucketToken;
const UserBucketHistory = db.UserBucketHistory;
const Token = db.Token;
const UserBucketTokenHistory = db.UserBucketTokenHistory;
const UserExchange = db.UserExchanges;

const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')
const getCurrentBucketValue = require('../helpers/current-bucket-price.helper.js')
const userAPI = require('../vauld-service-apis/user.js')

exports.buy = async (req, res) => {

    let transaction = false;    
    try {
        // get transaction
        transaction = await db.sequelize.transaction();
        
        //check for min value required for transaction Rs.1000
        if(req.body.buy_value<process.env.min_txn_value){
            await transaction.rollback()
            return res.send({'status':'fail','message':'Atleast Rs. '+process.env.VAULD_MIN_TXN_VALUE+'required for transaction'})
        }

        //Check for User balance from Vauld    
        let userBalanceExists=false;
        let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } });

        //vauld account balance check via API
        let response = await userAPI.userDetails(userExchange.user_exchange_identity)
        
        if(req.body.buy_value<=response.data.inrBalance.balance){
            userBalanceExists=true;
        }
/*
        if(!userBalanceExists){
            await transaction.rollback()
            return res.send({'status':'fail'    ,'message':'Low Account balance.'})
        }
*/
        //If User balance less than requested return

        //Get the bucket selected
        const bucket = await Bucket.findOne({include : {model: BucketToken, include: {model: Token}}, where: {id: req.body.bucket_id}})

            let bucketValue=0
            let bucketValueAt=moment()
            //let tokens = [];

        //get today's bucket value
        let result = await getCurrentBucketValue(bucket)
        console.log(result)

        if(result.status == 'success'){

            //console.log(bucketValue) //bucket value at current date and time
            
            //Calculate Bucket Quantity
            const bucketQty=req.body.buy_value/result.bucketValue;

            //Add investment to User Bucket (create new or add to existing if already created)
            //1. Add to/Create Master record for User Bucket
            //Add bucket wise quantity
            let userBucket = await UserBucket.findOne({where: {user_id: req.user.id,bucket_id: bucket.id}})
            
            if(!userBucket){
                userBucket = {
                    user_id : req.user.id,
                    bucket_id : bucket.id,
                    investment_value : req.body.buy_value,
                    bucket_quantity : bucketQty
                };
                userBucket = await UserBucket.create(userBucket,{transaction: transaction})
            } else{
                //add to investment value, update bucket_quantity

                userBucket.investment_value += req.body.buy_value
                userBucket.bucket_quantity += bucketQty
                await userBucket.save({transaction: transaction})
                
            }

            //Log to Bucket Transaction history
            let userBucketHistory={
                user_bucket_id : userBucket.id,
                order_id : Math.floor(Date.now() * Math.random()), 
                order_type : 'buy',
                inrAmount : req.body.buy_value,
                quantity : bucketQty
            }
            userBucketHistory = await UserBucketHistory.create(userBucketHistory,{transaction: transaction})
            
            //2. Add to/Create UserBucketToken records
            //Add token wise quantity
            for(let bucketToken of bucket.BucketTokens) {
                let tokenQtyBuy = bucketQty*bucketToken.quantity

                let userBucketToken=await UserBucketToken.findOne({where : {user_bucket_id: userBucket.id, token_id: bucketToken.token_id}})

                if(!userBucketToken){
                    userBucketToken = {
                        user_bucket_id: userBucket.id,
                        token_id: bucketToken.token_id,
                        quantity: tokenQtyBuy
                    }

                    userBucketToken = await UserBucketToken.create(userBucketToken,{transaction: transaction})
                } else{
                    //update quantity

                    userBucketToken.quantity += tokenQtyBuy
                    userBucketToken.save({transaction: transaction});
                }
                console.log(userBucketToken.token_id)
                console.log(tokenQtyBuy)
                console.log(bucketToken.Token.base_value_vauld)
                
                //Perform Transaction with Vauld. Rollback on any errors or failures

                const orderTokenAmount = tokenQtyBuy*bucketToken.Token.base_value_vauld

                let cryptoPrice = await buySellModuleAPI.getPrice(orderTokenAmount,'buy',bucketToken.Token.token_abbr.toLowerCase())
                console.log(cryptoPrice.data.quoteAmount)

                let responseOrder = await buySellModuleAPI.createOrder(userExchange.user_exchange_identity,'buy',bucketToken.Token.token_abbr.toLowerCase(),cryptoPrice.data.quoteAmount,orderTokenAmount)
                
                if(responseOrder.success){
                    //Order created at Vauld

                    //Log to Token Transaction history
                    let userBucketTokenHistory={
                        "user_bucket_id" : userBucket.id,
                        "token_id" : bucketToken.token_id,
                        "quantity" : tokenQtyBuy,
                        "order_type" : "buy", 
                        "order_id" : responseOrder.data.orderID, 
                        "inrAmount" : cryptoPrice.data.quoteAmount, 
                        "tokenAmount" :  orderTokenAmount
                    }
                    userBucketTokenHistory = await UserBucketTokenHistory.create(userBucketTokenHistory,{transaction: transaction})
                    

                    let responseOrderStatus = false;

                    do{

                        //Check status of Order at Vauld
                        responseOrderStatus = await buySellModuleAPI.orderStatus(userExchange.user_exchange_identity,responseOrder.data.orderID)
                        console.log(responseOrderStatus)

                    } while(!responseOrderStatus.success||responseOrderStatus.data.data.status!='complete')


                    
                } else {
                    //Order creation failed at Vauld

                    await transaction.rollback()
                    return res.status(500).send({
                        message:
                            'Error at order for one of the tokens'
                    });
                }

                
            }
        }  else{
            await transaction.rollback()
            return res.status(500).send({
                message:
                    result.message
            });
        }
        
        // commit
        await transaction.commit();
        return res.send({'status':'success'})
    } catch (err) {
        console.log(err)
        // Rollback transaction 
        if (transaction) await transaction.rollback();
    }
    

}

exports.sell = async (req, res) => {

    let transaction = false;    
    try {
        // get transaction
        transaction = await db.sequelize.transaction();

        //check for min value required for transaction Rs.1000
        if(req.body.sell_value<process.env.min_txn_value){
            await transaction.rollback()
            return res.send({'status':'fail','message':'Atleast Rs. '+process.env.VAULD_MIN_TXN_VALUE+'required for transaction'})
        }

        //Check for User balance from Vauld    
        let userBalanceExists=false;
        let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } });

        //vauld account balance check via API
        let response = await userAPI.userDetails(userExchange.user_exchange_identity)
        
        if(req.body.sell_value<=response.data.inrBalance.balance){
            userBalanceExists=true;
        }
/*
        if(!userBalanceExists){
            await transaction.rollback()
            return res.send({'status':'fail','message':'Low Account balance.'})
        }
*/

        //get bucket of user
        let userBucket = await UserBucket.findOne({where: {id: req.body.user_bucket_id, user_id: req.user.id}})
        //get portfolio bucket
        const bucket = await Bucket.findOne({include : {model: BucketToken, include: {model: Token}}, where: {id: userBucket.bucket_id}})

        //get today's bucket value
        let result = await getCurrentBucketValue(bucket,"sell")
        console.log(result)

        if(result.status == 'success'){

            //Calculate bucket quantity to be sold
            const bucketQtySold=req.body.sell_value/result.bucketValue

            //Check if individual tokens quantity to be sold is present in the user's bucket token
            for(let bucketToken of bucket.BucketTokens) {

                let userBucketToken=await UserBucketToken.findOne({where : {user_bucket_id: userBucket.id, token_id: bucketToken.token_id}})

                if(userBucketToken.quantity<(bucketQtySold*bucketToken.quantity)){
                    await transaction.rollback()
                    return res.send({
                        message:
                            'Token Quantity balance is low for selling - '+bucketToken.Token.token_name
                    });
                }
            }

            userBucket.bucket_quantity -= bucketQtySold
            userBucket.investment_value -= req.body.sell_value
            await userBucket.save({transaction: transaction})

            //Log to Bucket Transaction history
            let userBucketHistory={
                user_bucket_id : userBucket.id,
                order_id : Math.floor(Date.now() * Math.random()),
                order_type : 'sell',
                inrAmount : req.body.sell_value,
                quantity : bucketQtySold
            };

            userBucketHistory = await UserBucketHistory.create(userBucketHistory,{transaction: transaction})

            //Sell individual tokens based on bucket sell quantity
            for(let bucketToken of bucket.BucketTokens) {
                    
                let userBucketToken=await UserBucketToken.findOne({where : {user_bucket_id: userBucket.id, token_id: bucketToken.token_id}})

                let tokenQtySold = bucketQtySold*bucketToken.quantity
                userBucketToken.quantity -= tokenQtySold
                userBucketToken.save({transaction: transaction});
                
                const orderTokenAmount = tokenQtySold*bucketToken.Token.base_value_vauld
                
                let cryptoPrice = await buySellModuleAPI.getPrice(orderTokenAmount,'sell',bucketToken.Token.token_abbr.toLowerCase())
                console.log(orderTokenAmount)
                
                //Perform Transaction with Vauld. Rollback on any errors or failures
                
                let responseOrder = await buySellModuleAPI.createOrder(userExchange.user_exchange_identity,'sell',bucketToken.Token.token_abbr,cryptoPrice.data.quoteAmount,orderTokenAmount) 
                console.log(responseOrder)
                if(responseOrder.success){
                    //Order created at Vauld
                    
                    //Log to Token Transaction history
                    let userBucketTokenHistory={
                        "user_bucket_id" : userBucket.id,
                        "token_id" : bucketToken.token_id,
                        "quantity" : tokenQtySold,
                        "order_type" : "sell", //replace this from api result
                        "order_id" : responseOrder.data.orderID, //replace this from api result
                        "inrAmount" : cryptoPrice.data.quoteAmount, //replace with actual value
                        "tokenAmount" :  orderTokenAmount //replace with actual value
                    }
                    userBucketTokenHistory = await UserBucketTokenHistory.create(userBucketTokenHistory,{transaction: transaction})
                    console.log(userBucketHistory)

                    let responseOrderStatus = false;

                    do{

                        //Check status of Order at Vauld
                        console.log('loop')
                        responseOrderStatus = await buySellModuleAPI.orderStatus(userExchange.user_exchange_identity,responseOrder.data.orderID)
                        console.log(responseOrderStatus)

                    } while(!responseOrderStatus.success||responseOrderStatus.data.data.status!='complete')
                
                }
            }

        } else{
            await transaction.rollback()
            return res.status(500).send({
                message:
                    result.message
            });
        }

        // commit
        await transaction.commit();
        return res.send({'status':'success'})
    } catch (err) {
        console.log(err)
        // Rollback transaction 
        if (transaction) await transaction.rollback();
    }
    
}

exports.findOne = (req, res) => {
  
};

exports.findAll = (req, res) => {

};

