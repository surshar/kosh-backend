const db = require("../models");
const User = db.User;
const UserExchangeDeposit = db.UserExchangeDeposit;
const UserExchangeWithdrawBank = db.UserExchangeWithdrawBank;
const UserExchangeWithdrawal = db.UserExchangeWithdrawal;
const UserExchange = db.UserExchanges;
const dwe = require('../vauld-service-apis/deposit-withdraw-exchange.js');

exports.depositMoney = async(req, res, next) => {
    let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
    
    let response = await dwe.submitDepositConfirmation(userExchange.user_exchange_identity, req.body.paymentId, req.body.amount)

    if(!response.success){
        return res.send(response)
    }
    let userExchangeDeposit = {
        "exchange_id" : 1,
        "user_id" : req.user.id,
        "paymentId" : req.body.paymentId,  //RTGS/IMPS/NEFT reference Id
        "amount" : req.body.amount,
        "txnId" : response.txnID,
        "status" : response.txnID
    }

    await UserExchangeDeposit.create(userExchangeDeposit)

    return res.send(response)
}

exports.addBank = async(req, res, next) => { //Add bank for withdrawing money to
    let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
    
    let response = await dwe.addBankAccount(userExchange.user_exchange_identity, req.body.accountName, req.body.accountNumber, req.body.ifsc)

    console.log(response)

    if(!(response.success&&(response.data.status == "approved"||response.data.status == "pendingManual"))){
        return res.send(response);
    }

    let userExchangeWithdrawBank = {
        "exchange_id" : 1,
        "user_id" : req.user.id,
        "name" : req.body.accountName,
        "accountNumber" : req.body.accountNumber,
        "IFSC" : req.body.ifsc,
        "account_id_with_exchange" : response.data.accountID,
        "account_status_with_exchange" : response.data.status,
        "account_error_with_exchange" : response.data.error //If rejected
    }

    await UserExchangeWithdrawBank.create(userExchangeWithdrawBank)
    
    //return res.send({"status":"success","data": userExchangeWithdrawBank,"message":"Withdraw Bank added succesfully"})

    return res.send(response)
}


exports.removeBank = async(req, res) => {
    //Remove bank from withdrawal banks list

    let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
    
    let response = await dwe.removeBankAccount(userExchange.user_exchange_identity, req.body.bankAccountID);

    if(!response.success){
        return res.send(response)
    }

    let userExchangeWithdrawBank = await UserExchangeWithdrawBank.findOne({ where: { user_id: req.user.id, exchange_id: 1, account_id_with_exchange: req.body.bankAccountID } }); 

    await userExchangeWithdrawBank.destroy()

    return res.send(response)
}

exports.withdrawMoney = async(req, res, next) => { //Withdraw money to the specified bank (from added)
    let userExchange = await UserExchange.findOne({ where: { user_id: req.user.id, exchange_id: 1 } }); 
    
    let response = await dwe.requestWithdrawal(userExchange.user_exchange_identity, req.body.bankAccountID, req.body.amount)

    if(!response.success){
        return res.send(response)
    }

    let userExchangeWithdrawBank = await UserExchangeWithdrawBank.findOne({ where: { user_id: req.user.id, exchange_id: 1, account_id_with_exchange: req.body.bankAccountID } }); 

    let userExchangeWithdrawal = {
        "user_id" : req.user.id,
        "exchange_id" : 1,
        "user_exchange_withdraw_bank_id" : userExchangeWithdrawBank.id,
        "amount" : req.body.amount,
        "txnID" : response.txnID,
        "message" : response.message, 

    }

    await UserExchangeWithdrawal.create(userExchangeWithdrawal)
    
    //return res.send({"status":"success","data": userExchangeWithdrawal,"message":"Withdraw Bank added succesfully"})
    return res.send(response)
}
