'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bucket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Exchange, {foreignKey: 'exchange_id', foreignKeyConstraint: true});
      this.belongsTo(models.User, {foreignKey: 'created_by', foreignKeyConstraint: true});
      this.belongsTo(models.User, {foreignKey: 'last_updated_by', foreignKeyConstraint: true});
      this.hasMany(models.BucketToken, {foreignKey: 'bucket_id', foreignKeyConstraint: true});
    }
  };
  Bucket.init({
    version_no: DataTypes.STRING,
    bucket_name: DataTypes.STRING,
    bucket_image: DataTypes.STRING,
    bucket_description: DataTypes.TEXT,
    exchange_id: DataTypes.BIGINT.UNSIGNED,
    old_flag: DataTypes.BOOLEAN,
    versions: DataTypes.TEXT,
    created_by: DataTypes.BIGINT.UNSIGNED,
    last_updated_by: DataTypes.BIGINT.UNSIGNED
  }, {
    sequelize,
    modelName: 'Bucket',
  });
  return Bucket;
};