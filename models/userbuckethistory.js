'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBucketHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserBucket, {foreignKey: 'user_bucket_id', foreignKeyConstraint: true});
    }
  };
  UserBucketHistory.init({
    user_bucket_id: DataTypes.BIGINT.UNSIGNED,
    order_id: DataTypes.STRING,
    order_type: DataTypes.STRING,
    inrAmount: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('inrAmount');
        return value === null ? null : parseFloat(value);
      }
    },
    quantity: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('quantity');
        return value === null ? null : parseFloat(value);
      }
    }
  }, {
    sequelize,
    modelName: 'UserBucketHistory',
  });
  return UserBucketHistory;
};