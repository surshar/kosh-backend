'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.UserBucket, {foreignKey: 'user_id', foreignKeyConstraint: true});
    }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    otp: DataTypes.STRING,
    otp_attempts: DataTypes.INTEGER,
    otp_expiry: DataTypes.DATE,
    dob: DataTypes.DATEONLY,
    mobile_no: DataTypes.STRING,
    user_verified: DataTypes.BOOLEAN,
    email_verification_code: DataTypes.STRING,
    email_verification_code_expiry: DataTypes.DATE,
    user_email_verified: DataTypes.BOOLEAN
    }, 
    {
      sequelize,
      modelName : 'User',
      defaultScope: {
        attributes: { exclude: ['password'] },
      },
      indexes: [{unique:true, fields: ["email"]},{unique:true, fields: ["mobile_no"]}]
    
  });
  return User;
  
};
