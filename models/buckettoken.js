'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BucketToken extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Bucket, {foreignKey: 'bucket_id', foreignKeyConstraint: true});
      this.belongsTo(models.Token, {foreignKey: 'token_id', foreignKeyConstraint: true});
    }
  };
  BucketToken.init({
    bucket_id: DataTypes.BIGINT.UNSIGNED,
    token_id: DataTypes.BIGINT.UNSIGNED,
    quantity: {type: DataTypes.DECIMAL(50,20),
      get() {
        
        const value = this.getDataValue('quantity');
        return value === null ? null : parseFloat(value);
      }
    },
  }, {
    sequelize,
    modelName: 'BucketToken',
  });
  return BucketToken;
};