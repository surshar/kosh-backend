'use strict';

const {  Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBucket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.Bucket, {foreignKey: 'bucket_id', foreignKeyConstraint: true});
      this.belongsTo(models.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
      this.hasMany(models.UserBucketToken, {foreignKey: 'user_bucket_id', foreignKeyConstraint: true});
      this.hasMany(models.UserBucketTokenHistory, {foreignKey: 'user_bucket_id', foreignKeyConstraint: true});
      this.hasMany(models.UserBucketHistory, {foreignKey: 'user_bucket_id', foreignKeyConstraint: true});
    }
  };
  UserBucket.init({
    user_id: DataTypes.BIGINT.UNSIGNED,
    bucket_id: DataTypes.BIGINT.UNSIGNED,
    investment_value: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('investment_value');
        return value === null ? null : parseFloat(value);
      }
    },
    bucket_quantity: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('bucket_quantity');
        return value === null ? null : parseFloat(value);
      }
    }
  }, {
    sequelize,
    modelName: 'UserBucket',
  });

  //UserBucket.belongsTo(db.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
  return UserBucket;
};