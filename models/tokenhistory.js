'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TokenHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Token, {foreignKey: 'token_id', foreignKeyConstraint: true});
    }
  };
  TokenHistory.init({
    token_id: DataTypes.BIGINT.UNSIGNED,
    current_value: DataTypes.DECIMAL(50,20)
  }, {
    sequelize,
    modelName: 'TokenHistory',
  });
  return TokenHistory;
};