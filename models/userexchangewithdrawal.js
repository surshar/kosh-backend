'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserExchangeWithdrawal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserExchangeWithdrawal.init({
    user_id: DataTypes.BIGINT.UNSIGNED,
    exchange_id: DataTypes.BIGINT.UNSIGNED,
    user_exchange_withdraw_bank_id: DataTypes.BIGINT.UNSIGNED,
    amount: {type: DataTypes.DECIMAL(50,20),
      get() {
        
        const value = this.getDataValue('amount');
        return value === null ? null : parseFloat(value);
      }
    },
    txnID: DataTypes.STRING,
    message: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserExchangeWithdrawal',
  });
  return UserExchangeWithdrawal;
};