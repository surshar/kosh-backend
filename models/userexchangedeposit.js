'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserExchangeDeposit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserExchangeDeposit.init({
    exchange_id: DataTypes.BIGINT.UNSIGNED,
    user_id: DataTypes.BIGINT.UNSIGNED,
    paymentId: DataTypes.STRING,
    amount: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('amount');
        return value === null ? null : parseFloat(value);
      }
    },
    txnId: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserExchangeDeposit',
  });
  return UserExchangeDeposit;
};