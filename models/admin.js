'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Admin.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    otp: DataTypes.STRING,
    otp_attempts: DataTypes.INTEGER,
    otp_expiry: DataTypes.DATE,
    admin_status: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Admin',
    defaultScope: {
      attributes: { exclude: ['password'] },
    },
    indexes: [{unique:true, fields: ["email"]}]
  });
  return Admin;
};