'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Exchange extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {foreignKey: 'created_by', foreignKeyConstraint: true});
      this.belongsTo(models.User, {foreignKey: 'last_updated_by', foreignKeyConstraint: true});
    }
  };
  Exchange.init({
    exchange_name: DataTypes.STRING,
    exchange_description: DataTypes.TEXT,
    test_api_key: DataTypes.STRING,
    production_api_key: DataTypes.STRING,
    org_id: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    created_by: DataTypes.BIGINT.UNSIGNED,
    last_updated_by: DataTypes.BIGINT.UNSIGNED,
  }, {
    sequelize,
    modelName: 'Exchange',
  });
  return Exchange;
};