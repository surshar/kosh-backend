'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserExchanges extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
      this.belongsTo(models.Exchange, {foreignKey: 'exchange_id', foreignKeyConstraint: true});
      
    }
  };
  UserExchanges.init({
    user_id: DataTypes.BIGINT.UNSIGNED,
    exchange_id: DataTypes.BIGINT.UNSIGNED,
    user_identity: DataTypes.STRING,
    user_exchange_identity: DataTypes.STRING,
    user_exchange_kyc_status: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'UserExchanges',
  });
  
  return UserExchanges;
};