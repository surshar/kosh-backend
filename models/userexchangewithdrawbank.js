'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserExchangeWithdrawBank extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserExchangeWithdrawBank.init({
    exchange_id: DataTypes.BIGINT.UNSIGNED,
    user_id: DataTypes.BIGINT.UNSIGNED,
    name: DataTypes.STRING,
    accountNumber: DataTypes.STRING,
    IFSC: DataTypes.STRING,
    account_id_with_exchange: DataTypes.STRING,
    account_status_with_exchange: DataTypes.STRING,
    account_error_with_exchange: DataTypes.STRING,
    is_deleted: DataTypes.BOOLEAN

  }, {
    sequelize,
    modelName: 'UserExchangeWithdrawBank',
  });
  return UserExchangeWithdrawBank;
};