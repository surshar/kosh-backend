'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Token extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {foreignKey: 'created_by', foreignKeyConstraint: true});
      this.belongsTo(models.User, {foreignKey: 'last_updated_by', foreignKeyConstraint: true});
    }
  };
  Token.init({
    token_name: DataTypes.STRING,
    token_image: DataTypes.STRING,
    token_abbr: DataTypes.STRING,
    base_value_vauld: DataTypes.BIGINT.UNSIGNED,
    token_description: DataTypes.STRING,
    created_by: DataTypes.BIGINT.UNSIGNED,
    last_updated_by: DataTypes.BIGINT.UNSIGNED
  }, {
    sequelize,
    modelName: 'Token',
  });
  return Token;
};