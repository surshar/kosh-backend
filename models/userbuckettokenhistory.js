'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBucketTokenHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserBucket, {foreignKey: 'user_bucket_id', foreignKeyConstraint: true});
      this.belongsTo(models.Token, {foreignKey: 'token_id', foreignKeyConstraint: true});
    }
  };
  UserBucketTokenHistory.init({
    user_bucket_id: DataTypes.BIGINT.UNSIGNED,
    token_id: DataTypes.BIGINT.UNSIGNED,
    quantity: {type: DataTypes.DECIMAL(50,20),
      get() {
        // Workaround until sequelize issue #8019 is fixed
        const value = this.getDataValue('quantity');
        return value === null ? null : parseFloat(value);
      }
    },
    order_type: DataTypes.STRING,
    order_id: DataTypes.STRING,
    inrAmount: {type: DataTypes.DECIMAL(50,20),
      get() {

        const value = this.getDataValue('inrAmount');
        return value === null ? null : parseFloat(value);
      }
    },
    tokenAmount: DataTypes.DECIMAL(50,20)
  }, {
    sequelize,
    modelName: 'UserBucketTokenHistory',
  });
  return UserBucketTokenHistory;
};