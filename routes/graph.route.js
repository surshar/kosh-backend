const { Router } = require("express");

module.exports = app => {
    const graphs = require("../controllers/graph.controller.js");
  
    var router = require("express").Router();

    router.post("/bucket", graphs.bucketGraph);
    router.post("/token", graphs.tokenGraph);

    app.use(process.env.baseURL+'/api/graphs', router);
}