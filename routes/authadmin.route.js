const { Router } = require("express");
const authAdminCheck = require("../middleware/authadmin.js");

module.exports = app => {
    const authAdmin = require("../controllers/authadmin.controller.js");
  
    var router = require("express").Router();

    //Admin Login
    router.post("/login", authAdmin.login);

    //RESET PASSWORD
    router.post("/reset-password",authAdmin.resetPassword)
    
    router.post("/change-password",authAdmin.changePassword)

    app.use(process.env.baseURL+'/api/auth/admin', router);
}