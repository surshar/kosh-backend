const { Router } = require("express");

const auth = require("../middleware/auth.js");

module.exports = app => {
    const dwe = require("../controllers/depositwithdrawexchange.controller.js");
  
    var router = require("express").Router();

    router.post("/deposit-money",  auth , dwe.depositMoney);
    router.post("/add-withdraw-bank",  auth , dwe.addBank);
    router.post("/remove-withdraw-bank",  auth , dwe.removeBank);
    router.post("/withdraw-money",  auth , dwe.withdrawMoney);

    app.use(process.env.baseURL+'/api/deposit-withdraw', router);
}