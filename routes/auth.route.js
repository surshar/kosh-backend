const { Router } = require("express");
const authCheck = require("../middleware/auth.js");
const{ userValidatorResult,userValidator} = require("../Validation/Uservalidation")

module.exports = app => {
    const auth = require("../controllers/auth.controller.js");
  
    var router = require("express").Router();

    // Register
    router.post("/register",userValidator, userValidatorResult,auth.register);

    //Verify
    router.post("/verify", authCheck, auth.verifyUser);

    //RESET PASSWORD
    router.post("/reset-password",auth.resetPassword)
    
    router.post("/change-password",auth.changePassword)

    //Generate OTP again
    router.post('/generate-otp',auth.generateOTP)

    //Login
    router.post("/login", auth.login);

    //logout
    router.post("/logout", authCheck, auth.logout);

    //token
    router.post("/token", authCheck, auth.token);
    
    //verify email
    router.get("/verify-email", auth.verifyEmail);

    router.get('/test', auth.test)

    app.use(process.env.baseURL+'/api/auth', router);

    return router
}