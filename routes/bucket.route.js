const { Router } = require("express");
const authAdmin = require("../middleware/authadmin.js");
const moment = require('moment');

//Cron code
//Bucket Value cron code
const bucketsCron = require("../jobs/bucket-prices.job.js")
const bucketsCronRotational = require("../jobs/bucket-prices-rotational.job.js")
var multer = require('multer');

var storage = multer.diskStorage({   
    destination: function(req, file, cb) { 
        folderName = req.folderName = 'bucket-images'
       cb(null, './uploads/public/'+folderName);    
    }, 
    filename: function (req, file, cb) { 
       cb(null , moment().unix()+'-'+file.originalname);   
    }
 });
 var upload = multer({ storage: storage });

//Token Value cron code
const tokensCron = require("../jobs/token-prices.job.js")
const tokensCronRotational = require("../jobs/token-prices-rotational.job.js")

const cron = require('node-cron');

cron.schedule('0 0 * * *', () => { //Every day at midnight
    bucketsCron()
    tokensCron()
});

cron.schedule('*/10 * * * *', () => { //At every 10th minute
    bucketsCronRotational()
    tokensCronRotational()
});

//Cron code ends

module.exports = app => {
    const bucket = require("../controllers/bucket.controller.js");
  
    var router = require("express").Router();

    // Bucket Create
    router.post("/create",authAdmin, bucket.create);

    router.post('/add-bucket-image', authAdmin, upload.single('bucket_image'), bucket.addBucketImage)

    //Bucket View
    router.post("/view",bucket.findOne);

    //Buckets View All
    router.post("/view-all",bucket.findAll);

    app.use(process.env.baseURL+'/api/buckets', router);
}