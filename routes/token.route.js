const { Router } = require("express");

module.exports = app => {
    const tokens = require("../controllers/token.controller.js");
  
    var router = require("express").Router();

    router.get("/view-all",  tokens.findAll);

    app.use(process.env.baseURL+'/api/tokens', router);
}