const auth = require("../middleware/auth.js");
var multer = require('multer')
var upload = multer({ storage: multer.memoryStorage() })  // 1

module.exports = app => {
    const users = require("../controllers/user.controller.js");
  
    var router = require("express").Router();
  
    // Create a new user
    router.post("/", users.create);
  
    // Retrieve all users
    router.get("/", auth , users.findAll);

    //User KYC initiate
    router.post('/initiate-kyc', auth, users.intiateKYC)   

    //Verify Selfie
    router.post('/verify-selfie', auth, users.verifySelfie)   

    //Verify Document
    router.post('/verify-document', auth, users.verifyDocument)
    
    //Instant KYC approval
    router.post('/instant-kyc-approval', auth, users.instantKYCApproval)

    //upload test
    router.post('/upload-kyc-document', auth, upload.single('image'), users.uploadKYCDocument)

    //Retrieve User Exchange details
    router.get('/user-exchange-details', auth, users.userExchangeDetails)
    
    // Retrieve a single user with id
    router.get("/:id", users.findOne);
  
    // Update a user with id
    router.put("/:id", users.update);

    app.use(process.env.baseURL+'/api/users', router);
  };