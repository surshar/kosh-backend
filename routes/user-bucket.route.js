const auth = require("../middleware/auth.js");

module.exports = app => {
    const userBuckets = require("../controllers/userbucket.controller.js");
  
    var router = require("express").Router();

    router.post("/buy",  auth , userBuckets.buy);
    router.post("/sell",  auth , userBuckets.sell);

    app.use(process.env.baseURL+'/api/user-buckets', router);
}