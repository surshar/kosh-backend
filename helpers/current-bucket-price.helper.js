
const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')

module.exports = async function getCurrentBucketValue(bucket,type="buy"){
        let bucketValue=0
        //let tokens = [];

        //bucket.BucketTokens.forEach(bucketToken => {
        for (let bucketToken of bucket.BucketTokens) {
        
            //get price for total bucket quantity
            let cryptoPrice = await buySellModuleAPI.getPrice(bucketToken.quantity*bucketToken.Token.base_value_vauld,type,bucketToken.Token.token_abbr.toLowerCase())

            if(cryptoPrice.success){
                cryptoPrice=cryptoPrice.data

                bucketValue += cryptoPrice.quoteAmount


            } else{
                return {
                    message:
                        "Problem with "+bucketToken.Token.token_abbr+" not being found.",
                    status: "fail"
                };
            }

        }

        return {
            status: 'success', bucketValue: bucketValue
        }
    }
