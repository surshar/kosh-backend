const buySellModuleAPI = require('../vauld-service-apis/buy-sell-module.js')

module.exports = async function getCurrentTokenValue(token,type="buy"){

    //get price for single unit of token
    let cryptoPrice = await buySellModuleAPI.getPrice(token.base_value_vauld,type,token.token_abbr.toLowerCase())

    cryptoPriceQuote = null;
    if(cryptoPrice&&cryptoPrice.success){
        cryptoPriceQuote = cryptoPrice.data.quoteAmount
    }

    return {
        status: 'success', tokenValue: cryptoPriceQuote
    }

    
}